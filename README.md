# D&B Direct 2.0 SDK

The D&B Direct SDK is made up of a Javascript library designed to simplify
integration of D&B Direct 2.0 into web-based applications.

## Requirements

The Javascript library uses a server-side proxy to facilite communication to
the D&B Direct 2.0 API.

A PHP-based proxy is included. It requires PHP 5.3 or newer.

On the client-side, jQuery and Knockout are used. Most modern browsers will
work fine.

## Installation

1. Ensure PHP is running on your server.
2. Copy this entire repository to a folder under your webroot, for example
   /var/www/web-sdk
3. Open web-sdk/rest/settings.json and enter your D&B Direct username and
   password.
4. For best performance ensure web-sdk/rest/cache is writable by your web
   process. On xNIX-based systems, you can typically run:
    chmod 777 web-sdk/rest/cache
5. Browse to http://your-server/path/to/web-sdk/examples
